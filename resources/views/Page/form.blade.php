@extends('layout.master')
@section('title')
Halaman form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <form action="/welcome" method="post">
       @csrf
        <label>First Name:</label><br>
        <input type="text" name="name"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="text"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="01">Male<br>
        <input type="radio" name="gender" value="02">Female<br>
        <input type="radio" name="gender" value="03">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporian">Singaporian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken</label><br>
        <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="Language Spoken">English<br>
        <input type="checkbox" name="Language Spoken">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
           <input type="submit" value="Sign Up">
    </form>
@endsection